import json

# Input blocks report generated from Minecraft
blocks_report_path = "processing/json/mc/blocks.json"

# Input language file from Minecraft
lang_file_path = "processing/json/mc/en_us.json"

# Output material file after conversion to WS material file
material_blocks_path = "processing/json/blocks.json"



def get_displayname(lang, material_key):
    l_key = "block:" + material_key
    l_key = l_key.replace(":", ".")
    return lang.get(l_key)

def get_defaultstate(states):
    for state in states:
        if "default" in state:
            return state

def order_dictmove(material_in, material_out, key):
    if key in material_in:
        material_out[key] = material_in.pop(key)
    return material_out

def order_materialkeys(material):
    material = material.copy()
    material_ = {}
    order_dictmove(material, material_, "displayName")
    order_dictmove(material, material_, "color")
    order_dictmove(material, material_, "texture")
    order_dictmove(material, material_, "tags")
    order_dictmove(material, material_, "isAir")
    order_dictmove(material, material_, "tileEntity")
    order_dictmove(material, material_, "tileTick")

    for key in material:
        material_[key] = material[key]

    return material_



with open(blocks_report_path, "r") as blocks_file:
    blocks = json.load(blocks_file)

with open(lang_file_path, "r") as lang_file:
    lang = json.load(lang_file)

# Build MC materials profile from blocks report
materials_minecraft = {}
for m_key, m_value in blocks.items():

    # Set display name
    displayName = get_displayname(lang, m_key)
    if displayName != None:
        m_value["displayName"] = displayName
    else:
        print(f"No display name assigned: {m_key}")
    
    # Cleanup states
    if len(m_value["states"]) == 1:
        # Don't define state when there is only one
        del m_value["states"]
    else:
        # Define only the default state by default
        defaultState = get_defaultstate(m_value["states"])
        del defaultState["id"]
        m_value["states"] = [defaultState]

    # Order material keys
    m_value = order_materialkeys(m_value)

    materials_minecraft[m_key] = m_value

# Make materials definition and export
materials = { "minecraft": materials_minecraft }
with open(material_blocks_path, "w") as materials_blocks_file:
    json.dump(materials, materials_blocks_file, indent=4)
    
# print(json.dumps(materials, indent=4))